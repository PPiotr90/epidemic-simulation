import { Component, OnInit } from '@angular/core';
import {Simulation} from "../Simulation";
import {SimulationService} from "../simulation.service";
import {ActivatedRoute} from "@angular/router";
import {FormBuilder, Validators} from "@angular/forms";
import { Location } from '@angular/common';

@Component({
  selector: 'app-simulation-edit',
  templateUrl: './simulation-edit.component.html',
  styleUrls: ['./simulation-edit.component.css']
})
export class SimulationEditComponent implements OnInit {
  simulation: Simulation;
  simulationForm;

  constructor(private simulationService: SimulationService,
              private route: ActivatedRoute,
              private  formBuilder: FormBuilder,
              private  location : Location) {
    this.simulationForm = formBuilder.group({
      'name': [null, Validators.required],
      'population': [null, Validators.compose([Validators.pattern("[0-9]*"), Validators.required])],
      'infectedPopulation': [null, Validators.compose([Validators.required, Validators.min(1)])],
      'relation': [null, Validators.required ],
      'mortality': [null, Validators.compose([Validators.required, Validators.max(1)])],
      'timeToRecovery': [null, Validators.compose([Validators.required, Validators.min(1)]) ],
      'timeToMortality': [null,Validators.compose([Validators.required, Validators.min(1)]) ],
      'timeOfSimulation': [null,  Validators.compose([Validators.required, Validators.min(1)])]
    })
  }

  ngOnInit(): void {
    this.getSimulation()
  }

  getSimulation(): void {
    const id = +this.route.snapshot.paramMap.get('id');
    this.simulationService.getById(id)
      .subscribe(sim => {
        this.simulation = sim;
      console.log(sim)
      });

  }

  saveSimulation() {
    this.simulationService.put(this.simulation)
      .subscribe(() => location.assign('detail/'+this.simulation.id));
  }

}
