import { Injectable } from '@angular/core';
import {Simulation} from "./Simulation";
import {HttpClient, HttpHeaders} from "@angular/common/http";
import {SimulationDto} from "./SimulationDto";

@Injectable({
  providedIn: 'root'
})
export class SimulationService {


  private url = "http://localhost:8080/simulations";
  private httpOptions = {
    headers: new HttpHeaders().append('Content-Type', 'application/json; charset=utf-8')
  }
  constructor(private httpClient: HttpClient) {
  }
  get() {
    return this.httpClient.get<Simulation[]>(this.url,  this.httpOptions);
  }

  getById(id: number) {
    console.log(id);
    let address = this.url+"/"+id;
    return this.httpClient.get<Simulation>(address, this.httpOptions)
  }

  post(simulationDto: SimulationDto) {
    let simulationDtoInJson = JSON.stringify(simulationDto)

    return this.httpClient.post<Simulation>(this.url, simulationDtoInJson, this.httpOptions)
  }
  put(simulation : Simulation) {
    let simulationInJson = JSON.stringify(simulation)
    return this.httpClient.put<Simulation>(this.url, simulationInJson, this.httpOptions)
  }

  delete(id: number){
    return this.httpClient.delete(this.url+"/"+id, this.httpOptions)
  }





}
