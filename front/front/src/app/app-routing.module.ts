import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { CommonModule } from '@angular/common';
import {SimulationListComponent} from "./simulation-list/simulation-list.component";
import {SimulationViewComponent} from "./simulation-view/simulation-view.component";
import {SimulationEditComponent} from "./simulation-edit/simulation-edit.component";

const routes: Routes = [
  {path: '', component: SimulationListComponent},
  {path: 'detail/:id', component: SimulationViewComponent},
  {path: 'edit/:id', component: SimulationEditComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes), CommonModule],
  exports: [RouterModule],
  declarations: []
})
export class AppRoutingModule { }
