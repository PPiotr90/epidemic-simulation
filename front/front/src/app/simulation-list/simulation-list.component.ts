import { Component, OnInit } from '@angular/core';
import {SimulationService} from "../simulation.service";
import {Simulation} from "../Simulation";
import { Location } from '@angular/common';


@Component({
  selector: 'app-simulation-list',
  templateUrl: './simulation-list.component.html',
  styleUrls: ['./simulation-list.component.css']
})
export class SimulationListComponent implements OnInit {

 public simulations : Simulation[];
 public  createView: boolean

  constructor(private simulationService: SimulationService,
              private  location : Location) { }

  ngOnInit(): void {
    this.refreshList()

  }

  public  refreshList() {
    console.log("refreshing");
    this.simulationService.get().subscribe(simulations =>this.simulations = simulations);

  }
  public  create() {
   this.createView = !this.createView;
  }



  delete(id: number) {
   this.simulationService.delete(id)
     .subscribe(() => this.refreshList());


  }
}
