import { Component, OnInit } from '@angular/core';
import {Simulation} from "../Simulation";
import {SimulationService} from "../simulation.service";
import {ActivatedRoute} from "@angular/router";







@Component({
  selector: 'app-simulation-view',
  templateUrl: './simulation-view.component.html',
  styleUrls: ['./simulation-view.component.css']
})
export class SimulationViewComponent implements OnInit {
  simulation : Simulation;


  constructor(private simulationService: SimulationService,
              private  route: ActivatedRoute) { }

  ngOnInit(): void {
    this.getSimulation()
  }




  getSimulation(): void {
   const  id = +this.route.snapshot.paramMap.get('id');
    this.simulationService.getById(id)
      .subscribe(sim => {
        this.simulation = sim;
        let recovered: number[] = sim.dayOfSimulationList
          .map(day => day.recoveredPopulation)
        let infected: number [] = sim.dayOfSimulationList
          .map(day => day.InfectedPopulation)


      });
  }

  edit() {
    location.assign('edit/'+this.simulation.id)
  }

  backToList() {
    location.assign((''))
  }
}
