import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import {HttpClientModule} from "@angular/common/http";
import {RouterModule} from "@angular/router";

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { SimulationListComponent } from './simulation-list/simulation-list.component';
import { SimulationViewComponent } from './simulation-view/simulation-view.component';
import { SimulationEditComponent } from './simulation-edit/simulation-edit.component';
import { SimulationNewComponent } from './simulation-new/simulation-new.component';
import {FormsModule, ReactiveFormsModule} from "@angular/forms";

@NgModule({
  declarations: [
    AppComponent,
    SimulationListComponent,
    SimulationViewComponent,
    SimulationEditComponent,
    SimulationNewComponent
  ],
    imports: [
        HttpClientModule,
        BrowserModule,
        AppRoutingModule,
        ReactiveFormsModule,
        FormsModule,
    ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
