import {Component, OnInit, Output} from '@angular/core';
import {SimulationService} from "../simulation.service";
import {FormBuilder, Validators} from "@angular/forms";
import {EventEmitter} from "@angular/core";

@Component({

  selector: 'app-simulation-new',
  templateUrl: './simulation-new.component.html',
  styleUrls: ['./simulation-new.component.css']
})
export class SimulationNewComponent implements OnInit {

  simulationForm;

  constructor(private simulationService : SimulationService, formBuilder: FormBuilder) {
    this.simulationForm = formBuilder.group({
      'name': [null, Validators.required],
      'population': [null, Validators.compose([
        Validators.pattern("[0-9]*"),
        Validators.required])],
      'infectedPopulation': [null, Validators.compose([
        Validators.required,
        Validators.min(1),
        Validators.pattern("[0-9]*")])],
      'relation': [null, Validators.required ],
      'mortality': [null, Validators.compose([
        Validators.required,
        Validators.max(1)])],
      'timeToRecovery': [null, Validators.compose([
        Validators.required,
        Validators.min(1),
        Validators.pattern("[0-9]*")]) ],
      'timeToMortality': [null,Validators.compose([
        Validators.required,
        Validators.min(1),
        Validators.pattern("[0-9]*")])],
      'timeOfSimulation': [null,  Validators.compose([
        Validators.required,
        Validators.min(1),
        Validators.pattern("[0-9]*")])]
    });
  }

  ngOnInit(): void {
  }

  createSimulation() {
    if(this.simulationForm.valid) {
let simulationToSave= JSON.stringify(this.simulationForm.value)
      console.log(simulationToSave)
      this.simulationService.post(this.simulationForm.value).subscribe(()=> {

        location.reload()
      })
    }
    else
      alert("invalid data in form")
  }
}
