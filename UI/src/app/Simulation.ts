import {DayOfSimulation} from "./DayOfSimulation";

export  class  Simulation {
name: String;
 population : number;
 infectedPopulation : number;
relation : number;
mortality : number;
timeToRecovery : number;
timeToMortality : number;
 timeOfSimulation : number;
  dayOfSimulationList : DayOfSimulation[];
}
