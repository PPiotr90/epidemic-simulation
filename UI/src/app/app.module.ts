import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { SimulationListComponent } from './simulation-list/simulation-list.component';
import { SimulationViewComponent } from './simulation-view/simulation-view.component';
import { SimulationEditComponent } from './simulation-edit/simulation-edit.component';

@NgModule({
  declarations: [
    AppComponent,
    SimulationListComponent,
    SimulationViewComponent,
    SimulationEditComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
