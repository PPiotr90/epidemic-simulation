export  class  SimulationDto{
  population : number;
  infectedPopulation : number;
  relation : number;
  mortality : number;
  timeToRecovery : number;
  timeToMortality : number;
  timeOfSimulation : number;

}
