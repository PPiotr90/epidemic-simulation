package pl.pp.bsackend.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class SimulationDto {
    private String name;
    private  Long population;
    private  Long infectedPopulation;
    private  float relation;
    private  float mortality;
    private  int timeToRecovery;
    private  int timeToMortality;
    private  int  timeOfSimulation;
}
