package pl.pp.bsackend.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import pl.pp.bsackend.model.Simulation;

public interface SimulationRepository  extends JpaRepository<Simulation, Long> {
}
