package pl.pp.bsackend;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class BsackendApplication {

    public static void main(String[] args) {
        SpringApplication.run(BsackendApplication.class, args);
    }

}
