package pl.pp.bsackend.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.cassandra.CassandraProperties;
import org.springframework.web.bind.annotation.*;
import pl.pp.bsackend.dto.SimulationDto;
import pl.pp.bsackend.service.SimulationService;
import pl.pp.bsackend.model.Simulation;

import java.util.List;

@RestController
@CrossOrigin("*")
@RequestMapping("/simulations")
public class SimulationController {

    private SimulationService simulationService;

@Autowired
    SimulationController(SimulationService simulationService) {
        this.simulationService = simulationService;
    }

    @GetMapping
    public List<Simulation> findAll() {
        return simulationService.findAll();
    }
    @PostMapping
    public  Simulation save(@RequestBody SimulationDto simulationDto) {

        System.out.println(simulationDto);
        return  simulationService.save(simulationDto);
    }

    @PutMapping
    public  Simulation patch( @RequestBody Simulation simulation) {
        return simulationService.save(simulation);
    }
    @DeleteMapping("/{id}")
    public void  deleteById(@PathVariable("id") long id) {
        simulationService.deleteById(id);
    }
    @GetMapping("/{id}")
    public  Simulation getById(@PathVariable("id") Long id) {
    return simulationService.findById(id);
    }
}

