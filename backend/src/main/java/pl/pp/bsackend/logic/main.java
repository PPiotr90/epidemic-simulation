package pl.pp.bsackend.logic;

import pl.pp.bsackend.model.Simulation;

public class main {
    public static void main(String[] args) {
        Simulation simulation = new Simulation(1L, "my", 100000L,1L,
                1.1f, 0.25f, 7, 3,100 );

        CountSimulation.countDays(simulation);
    }
}
