package pl.pp.bsackend.logic;

import pl.pp.bsackend.model.DayOfSimulation;
import pl.pp.bsackend.model.Simulation;

import java.util.ArrayList;
import java.util.List;

public class CountSimulation {
    private static List<DayOfSimulation> listOfDays = new ArrayList<>();
    private static List<Long> infectedInDay = new ArrayList<>();

    public static List<DayOfSimulation> countDays(Simulation simulation) {
        listOfDays = new ArrayList<>();

        float relation = simulation.getRelation();
        float mortality = simulation.getMortality();
        int timeToMortality = simulation.getTimeToMortality();
        int timeToRecovery = simulation.getTimeToRecovery();
        DayOfSimulation first = getFirstDay(simulation);
        listOfDays.add(first);
        DayOfSimulation previousDay = first;
        infectedInDay.add(simulation.getInfectedPopulation());

        while (listOfDays.size() <= simulation.getTimeOfSimulation()) {

            DayOfSimulation newDay = new DayOfSimulation();
            long toDayDead = 0;
            long toDayRecovered = 0;
            long newInfected = 0;
            long othersInfected = 0L;


            if (listOfDays.size() > timeToMortality) {

                long infectedBeforeMortalityTime = getInfectedInDayBefore(timeToMortality);
                toDayDead = (long) (infectedBeforeMortalityTime * mortality);
            }
            if (listOfDays.size() > timeToRecovery) {

                long infectedBeforeRecoveryTime = getInfectedInDayBefore(timeToRecovery);
                long deadBeforeRecovery = 0L;
                if (timeToMortality < timeToRecovery) {
                    deadBeforeRecovery = (long) (infectedBeforeRecoveryTime * mortality);
                }
                toDayRecovered = infectedBeforeRecoveryTime - deadBeforeRecovery;
            }
                othersInfected = previousDay.getInfectedPopulation() - toDayDead - toDayRecovered;
                newInfected = (long) (othersInfected * relation);

            if (newInfected > previousDay.getSubjectPopulation()) {
                newInfected = previousDay.getSubjectPopulation();
            }
            long subjectPopulation = previousDay.getSubjectPopulation() - newInfected;


            newDay.setDeadPopulation(previousDay.getDeadPopulation() + toDayDead);
            newDay.setInfectedPopulation(newInfected + othersInfected);
            newDay.setRecoveredPopulation(previousDay.getRecoveredPopulation() + toDayRecovered);

            newDay.setSubjectPopulation(subjectPopulation);
            infectedInDay.add(newInfected);
            listOfDays.add(newDay);
            previousDay = newDay;



        }
        return listOfDays;

    }


    public static DayOfSimulation getFirstDay(Simulation simulation) {

        DayOfSimulation result = new DayOfSimulation();
        result.setInfectedPopulation(simulation.getInfectedPopulation());
        result.setDeadPopulation(0);
        result.setRecoveredPopulation(0);
        result.setSubjectPopulation(simulation.getPopulation() - simulation.getInfectedPopulation());
        return result;
    }

    private static long getInfectedInDayBefore(int daysBefore) {
        int numberOfDay = listOfDays.size() - daysBefore - 1;

        long infectedInDayBefore = infectedInDay.get(numberOfDay);

        return infectedInDayBefore;
    }
}
