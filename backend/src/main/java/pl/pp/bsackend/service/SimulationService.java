package pl.pp.bsackend.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import pl.pp.bsackend.dto.SimulationDto;
import pl.pp.bsackend.logic.CountSimulation;
import pl.pp.bsackend.model.DayOfSimulation;
import pl.pp.bsackend.model.Simulation;
import pl.pp.bsackend.repository.SimulationRepository;

import javax.persistence.EntityNotFoundException;
import java.util.List;

@Service
public class SimulationService {

    private SimulationRepository simulationRepository;

@Autowired
    SimulationService(SimulationRepository simulationRepository) {
        this.simulationRepository = simulationRepository;
    }

    public Simulation findById(long id) {
        return simulationRepository.findById(id)
                .orElseThrow(() ->
                        new EntityNotFoundException("no simulation with this id id base"));
    }

    public  Simulation save(SimulationDto simulationDto) {

        Simulation simulationToSave = createSimulationFromDto(simulationDto);
        List<DayOfSimulation> dayOfSimulations = CountSimulation.countDays(simulationToSave);
        simulationToSave.setDayOfSimulationList(dayOfSimulations);
        return simulationRepository.save(simulationToSave);

    }

    public Simulation save(Simulation simulationToSave) {
        List<DayOfSimulation> dayOfSimulations = CountSimulation.countDays(simulationToSave);
        simulationToSave.setDayOfSimulationList(dayOfSimulations);
        return simulationRepository.save(simulationToSave);

    }

    public  void  deleteById(Long id) {
        simulationRepository.deleteById(id);
    }

    public  List<Simulation> findAll() {
        return simulationRepository.findAll();
    }

    private Simulation createSimulationFromDto(SimulationDto source) {
        System.out.println(source);
        Simulation result = new Simulation();
       result.setName(source.getName());
       result.setPopulation(source.getPopulation());
       result.setInfectedPopulation(source.getInfectedPopulation());
       result.setRelation(source.getRelation());
       result.setMortality(source.getMortality());
       result.setTimeToRecovery(source.getTimeToRecovery());
       result.setTimeToMortality(source.getTimeToMortality());
       result.setTimeOfSimulation(source.getTimeOfSimulation());
        return  result;
    }
}
