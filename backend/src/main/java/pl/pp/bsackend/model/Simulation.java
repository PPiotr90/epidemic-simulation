package pl.pp.bsackend.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
@Table
@Data
@AllArgsConstructor
@NoArgsConstructor
public class Simulation {

    @Id
    @GeneratedValue
    private Long id;

    private String name;
    private  Long population;
    private  Long infectedPopulation;
    private  float relation;
    private  float mortality;
    private  int timeToRecovery;
    private  int timeToMortality;
    private  int  timeOfSimulation;

    @OneToMany(cascade = CascadeType.ALL, orphanRemoval = true)
    private List<DayOfSimulation> dayOfSimulationList = new ArrayList<>();


    public Simulation(Long id, String name, Long population, Long infectedPopulation, float relation, float mortality,
                      int timeToRecovery, int timeToMortality,
                      int timeOfSimulation) {
        this.id = id;
        this.name = name;
        this.population = population;
        this.infectedPopulation = infectedPopulation;
        this.relation = relation;
        this.mortality = mortality;
        this.timeToRecovery = timeToRecovery;
        this.timeToMortality = timeToMortality;
        this.timeOfSimulation = timeOfSimulation;

    }
}
