package pl.pp.bsackend.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table
@Data
@AllArgsConstructor
@NoArgsConstructor
public class DayOfSimulation {
    @Id
    @GeneratedValue
    private  Long id;

    private  long InfectedPopulation;
    private long subjectPopulation;
    private long deadPopulation;
    private  long recoveredPopulation;
}
